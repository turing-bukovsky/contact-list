import React from "react";
import "./App.css";
import ContactList from "./components/ContactList/ContactList";

const App: React.FC = () => {
  return (
    <div className="App">
      <h1>Turing app !!!</h1>
      <ContactList />
    </div>
  );
};

export default App;
