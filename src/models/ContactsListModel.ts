import { AlphabetEnum as Alphabet } from "./AlphabetEnum";

// mock set of contacts
// const CONTACTS = [
//     {firstName: "Ludek", lastName: "Bukovsky", company: "Turing", mobile: 736528467, mail: "ludek.bukovsky@turing.cz", homePage: "seznam.cz", birthDate: "17.8.1992", address: "Brno 614 00"},
//     {firstName: "Andrej", lastName: "Purschvalof", company: "Cetin", mobile: 6034758633, mail: "andrej@cetin.cz", homePage: "seznam.cz", birthDate: "8.8.1988", address: "Brno 602 00"},
//     {firstName: "Dominika", lastName: "Skacelova", company: "Burger-Inn", mobile: 800333121, mail: "burger-in@burger.cz", homePage: "burger.cz", birthDate: "1.5.1995", address: "Brno 612 00"},
//     {firstName: "Zdenek", lastName: "Opletal", company: "Snowboards-ca", mobile: 601856233, mail: "boards@boards.cz", homePage: "boards.cz", birthDate: "2.7.2001", address: "Olomouc 755 10"}
// ];

export class ContactList {}

/* Group is se alphabetically as ContactsGroup["A", "B", ...] so to each specific alphabet belongs
   list of ContactItem[] which is represented
   (for demonstrating used only First names.) as ContactItem[{Adam,...}, {Alice,...}, ...]
*/
export class ContactsGroup {
  private contactsGroupMap: Map<string, ContactItem[]>;

  constructor() {
    this.contactsGroupMap = new Map<string, ContactItem[]>();
  }

  initGroupContacts(contacts: ContactItem[]) {
    for (let letter in Alphabet) {
      let contactsOfEachLetter: ContactItem[] = [];
      contacts.forEach(contact => {
        if (letter === contact.getGroup()) {
          contactsOfEachLetter.push(contact);
          this.contactsGroupMap.set(letter, contactsOfEachLetter);
        }
      });
    }
  }

  getContactsGroup(): Map<string, ContactItem[]> {
    return this.contactsGroupMap;
  }

  addNewGroupContact(contact: ContactItem) {
    const firstLetterGroup = contact.getGroup();
    let contactOfLetter: ContactItem[] = [];
    for (let letter in Alphabet) {
      if (letter === firstLetterGroup) {
        contactOfLetter.push(contact);
        this.contactsGroupMap.set(letter, contactOfLetter);
      }
    }
  }
}

export class ContactItem {
  static _id: number = 0;
  public id: number;
  private group: string;

  constructor(
    private _firstName: string,
    private _lastName: string,
    private _mobile: number
  ) {
    this.id = ContactItem._id++;
    this.group = ContactItem.getGroupByFirstNameUppered(this.firstName);
  }

  public getId(): number {
    return this.id;
  }

  public getGroup(): string {
    return this.group;
  }

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName(): string {
    return this._lastName;
  }

  set lastName(value: string) {
    this._lastName = value;
  }

  get mobile(): number {
    return this._mobile;
  }

  set mobile(value: number) {
    this._mobile = value;
  }

  // class methods

  // todo add check if name is not empty and starts only with a-z | A-Z

  // TODO-- tady to delam tak zvlastne jelikoz mi kricel TS ze neprirazuji privatni group, avsak
  // TODO-- jsem ji prirazoval v metode ... takze obezlicka
  private static getGroupByFirstNameUppered(firstName: string): string {
    const groupLetter = firstName[0];
    return ContactItem.makeGroupToUpperIfAlreadyNot(groupLetter);
  }

  private static makeGroupToUpperIfAlreadyNot(letter: string): string {
    return letter.toUpperCase();
  }
}

// Class only for mocking contacts
export class ContactsMock {
  static mockInitialContacts(): ContactItem[] {
    let newContacts: ContactItem[] = [];

    let newContact = new ContactItem("Ludek", "Bukovsky", 736528467);
    newContacts.push(newContact);

    newContact = new ContactItem("Dominika", "Skacelova", 800333121);
    newContacts.push(newContact);

    newContact = new ContactItem("Andrej", "Purschvalof", 6034758633);
    newContacts.push(newContact);

    newContact = new ContactItem("Lubos", "Gajdziok", 736528767);
    newContacts.push(newContact);

    newContact = new ContactItem("Zdenek", "Opletal", 601856233);
    newContacts.push(newContact);

    newContact = new ContactItem("Zbynek", "Sykora", 602256233);
    newContacts.push(newContact);

    newContact = new ContactItem("Lubomir", "Muslicka", 666528767);
    newContacts.push(newContact);

    newContact = new ContactItem("Zuzana", "Machova", 772256233);
    newContacts.push(newContact);

    return newContacts;
  }
}
