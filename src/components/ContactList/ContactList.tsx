import React, { Component } from "react";
import {
  ContactItem,
  ContactsGroup,
  ContactsMock
} from "../../models/ContactsListModel";

// global variable
let contactsGroupObj = new ContactsGroup();

class ContactList extends Component<IContactListProps, IContactListState> {
  constructor(props: IContactListProps) {
    super(props);
    this.state = {
      searchedText: "",
      isEditingContact: false
    };

    let mockedContacts: ContactItem[] = ContactsMock.mockInitialContacts();
    contactsGroupObj.initGroupContacts(mockedContacts);

    this.displayContacts();
  }

  displayContacts() {
    let contactsGroups: Map<
      string,
      ContactItem[]
    > = contactsGroupObj.getContactsGroup();

    let displayContactResult = [];
    for (const [k, v] of contactsGroups.entries()) {
      displayContactResult.push(<ul key={k}>{k}</ul>);
      v.forEach((contact, index) => {
        displayContactResult.push(
          <li key={v[index].id}>{v[index].firstName + v[index].lastName}</li>
        );
      });
    }
    return displayContactResult;
  }

  render() {
    return (
      <div>
        <ul>{this.displayContacts()}</ul>
      </div>
    );
  }
}

export default ContactList;

interface IContactListProps {
  contacts?: JSX.Element;
  contactsGroup?: ContactsGroup;
}

interface IContactListState {
  searchedText: string;
  isEditingContact: boolean;
}
